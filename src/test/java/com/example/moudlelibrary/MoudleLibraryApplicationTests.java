package com.example.moudlelibrary;

import com.example.moudlelibrary.dao.*;
import com.example.moudlelibrary.dao.ModelingScenarioTemp.FileInfo;
import com.example.moudlelibrary.tools.DockerService;
import com.example.moudlelibrary.tools.FileOperator;
import com.example.moudlelibrary.tools.GeoServerOperation;
import com.example.moudlelibrary.tools.HttpUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.util.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MoudleLibraryApplicationTests {
    @Autowired
    private MongoDBOperator mongoDBOperator;

    @Autowired
    DockerService dockerService;

    @Test
    void contextLoads() throws Exception {
        System.out.println("测试开始");
//        // 指定要列出文件的文件夹路径
//        String folderPath = "E:\\models\\FDS";
//
//        // 创建一个File对象，表示要列出的文件夹
//        File folder = new File(folderPath);
//
//        // 调用递归方法来列出文件夹下所有文件的名称
//        String file_names = FileOpera.listFiles(folder);
//        LocalDateTime now = LocalDateTime.now();
//
//        // 自定义日期时间格式
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
//        String formattedDateTime = now.format(formatter);
//
//        String rootPath = "E:\\models\\FDS";
//        String[] fileNames = "room_fire.end,room_fire.out,room_fire.smv,room_fire_0001_00000002_00.q,room_fire_0001_00000004_00.q,room_fire_0001_00000006_00.q,room_fire_0001_00000008_00.q,room_fire_0001_00000010_00.q,room_fire_01.s3d,room_fire_01.s3d.sz,room_fire_02.s3d,room_fire_02.s3d.sz,room_fire_hrr.csv".split(",");
//        // 要创建的ZIP文件的路径和名称
//        String zipFilePath = rootPath + "\\"+formattedDateTime +"resultFile.zip";
//
//        // 要添加到ZIP文件的文件列表
//        for (int i = 0; i < fileNames.length; i++) {
//            fileNames[i] = rootPath + "\\" +fileNames[i];
//        }
//
//        try {
//            // 创建一个输出流，用于写入ZIP文件
//            FileOutputStream fos = new FileOutputStream(zipFilePath);
//            ZipOutputStream zipOut = new ZipOutputStream(fos);
//
//            for (String filePath : fileNames) {
//                // 打开要添加到ZIP文件的文件
//                FileInputStream fis = new FileInputStream(filePath);
//                // 创建ZIP条目，指定文件名
//                ZipEntry zipEntry = new ZipEntry(new File(filePath).getName());
//                zipOut.putNextEntry(zipEntry);
//
//                // 将文件内容写入ZIP文件
//                byte[] buffer = new byte[1024];
//                int length;
//                while ((length = fis.read(buffer)) > 0) {
//                    zipOut.write(buffer, 0, length);
//                }
//
//                // 关闭当前ZIP条目
//                zipOut.closeEntry();
//                fis.close();
//            }
//
//            // 关闭ZIP输出流
//            zipOut.close();
//            System.out.println("ZIP文件已创建成功：" + zipFilePath);
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        /** 数据库测试 */
//        Module module = new Module();
//        module.setId(UUID.randomUUID().toString());
//        module.setName("FDS");
//        module.setDescription("FDS Fire Modeling");
//        module.setType(ModuleType.Socioeconomics);
//
//        module.setKeyword(new String[]{"Disaster modeling", "fire "});
//        module.setProvider("Fengyuan Zhang");
//        module.setEmail("FengyuanZhang@163.com");
//        module.setCopyright("Fengyuan Zhang");
//
//        List<Map> inputs = new ArrayList<>();
//
//
//        Map temp1 = new HashMap();
//        temp1.put("name", "FDSData");
//        temp1.put("option", "fds");
//
//        temp1.put("description", "FDS Parameter File");
//
//        inputs.add(temp1);
//
//        module.setInputData(inputs);
//
//        List<Map> outputs = new ArrayList<>();
//
//        Map temp3 = new HashMap();
//        temp3.put("name", "Result");
//        temp3.put("option", "false");
//        temp3.put("type", "ZIP");
//        temp3.put("description", "FDS output file with indoor fire spread path, point of origin and point of extinguishment.");
//
//        outputs.add(temp3);
//
//        module.setOutputData(outputs);
//
//        module.setFileScheme(FileOperator.ReadTXTFile("E:\\ideaProjects\\MoudleLibrary\\modulelibrary\\FDS\\OMF_File.xml"));
//        module.setDataScheme(FileOperator.ReadTXTFile("E:\\ideaProjects\\MoudleLibrary\\modulelibrary\\FDS\\OMF_Data.xml"));
//        module.setCodeExample("from PyOIMF.ModuleLibrary.FDS.FDSModule import FDSModule\nif __name__ == '__main__':\n   FDS = FDSModule(r\"./workspace\", r\"./Data/FDSDemoBack.fds\")\n   FDS.initialize() \n   FDS.run() \n   print(FDS.getStatus()) \n   print(\"ok\")\n");
//        mongoDBOperator.insertDocument(module);
//        mongoDBOperator.delete(new ModelingScene(),"id","19396a6c-0d97-4dbb-ac05-0f3de27a1a14");
//        DockerContainer dockerContainer = (DockerContainer) mongoDBOperator.findOneByKey(new DockerContainer(),"id","18a62722-2dfe-4d9e-bc88-098628ee9b51");

//      Module module = (Module) mongoDBOperator.findOneByKey(new Module(),"id", "d731a8bd-2ccb-491a-ab06-0f97f59ea9c2");
//        System.out.println("ok");
      //        List<Module> modules = (List<Module>) mongoDBOperator.finAll(new Module());
//        String content ="";
//        try {
//            content = FileOperator.ReadTXTFile("E:\\PycharmProjects\\Python-based Open Modeling Framework\\ModuleLibrary\\FDS\\OMF_File.xml");
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//        Module module = (Module) mongoDBOperator.findOneByKey(new Module(),"id", "d731a8bd-2ccb-491a-ab06-0f97f59ea9c2");
//        DockerContainer container = new DockerContainer();
//        container.setId(UUID.randomUUID().toString());
//        container.setName("FDS");
//        mongoDBOperator.insertDocument(container);
//        DockerContainer container = (DockerContainer) mongoDBOperator.findOneByKey(new DockerContainer(),"name","name");
        boolean dck = dockerService.checkState("f4b850b13cf87039f71153f46198cb8755bed700416391c76cdde4da14baea5d");
        String authorityPost = "Public";
//        DockerContainer container = new DockerContainer();
//        container = new DockerContainer();
//        container.setName("testContainer");
//        container.setId(UUID.randomUUID().toString());
//        container.setImageName("Python 3.8");
////        String rootPath = "/home/vge/DockerContainer";
//        String rootPath = "E:/DockerContainer";
//        container.setVolumesDir(rootPath + "/test");
//        container.setWorkDir("test");
        String containerID = dockerService.createContainer("python:3.8-bullseye", "CREST", "E:/DockerContainer/CREST", "CREST");
//        container.setContainerID(containerID);

//        String pythonInterpreter = PythonInterpreter.Python2_7.getTag();
//        String code = "def print_hi(name):\n  print(f'Hello, {name}')\n\nif __name__ == '__main__':\n  print_hi('Hello world!')";
//        mongoDBOperator.update(new ModelingScene(),,"code",code);
//        dockerService.runCMD("68a57296edaccf7cc5f674cc9d5fe70bbff85ff20dc655323122b3fe72bc2421","pip install time123456");
//        String path ="E:\\DockerContainer\\FDS\\library_list.txt";
//        FileInputStream fileInputStream = new FileInputStream(path);
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
//        String line;
//        String content = "";
//        List<Map> pythonLibrary = new ArrayList<>();
//        bufferedReader.readLine();
//        bufferedReader.readLine();
//        while ((line = bufferedReader.readLine()) != null) {
//            String[] libPages =  line.split(" ");
//            Map libPage =new HashMap();
//            libPage.put("name",libPages[0]);
//            libPage.put("version",libPages[libPages.length -1]);
//            pythonLibrary.add(libPage);
//        }
//        fileInputStream.close();
//        System.out.println("******文件: " + path + " 读取成功！******");
//        List<Map> fileList = new ArrayList<>();
//        fileList = FileOperator.readFiles2EleTree("E:\\DockerContainer\\PyHEF\\workspace");
//        GeoServerOperation.PublishShape("PyHEF","wangjiaba_river_network","wangjiaba_river_network","river_stream","E:\\DockerContainer\\PyHEF\\workspace\\WatershedExtraction\\output\\wangjiaba_river_network.shp");
//        String sceneInfo = FileOperator.readTxtFile("E:\\DockerContainer\\" + "PyHEF" + "\\sceneinfo.txt");
//        JSONObject jsonObject = new JSONObject(sceneInfo);
//        String id = jsonObject.getString("id");
        // 构建Python脚本内容
//        String pythonScriptContent = "def print_hi(name):\n"
//                + "    print(f'Hello, {name}!')\n\n"
//                + "if __name__ == '__main__':\n"
//                + "    print_hi('word')";
//        dockerService.writeFileToContainer("49c821583107","main.py",pythonScriptContent);
//
//
//
//        FileInfo fileInfo = FileInfo.builder().filePath("E:\\DockerContainer\\PyHEF\\workspace").build();
//        List<FileInfo> filesTree;
//        filesTree = FileOperator.getFileDirectoryTree(fileInfo,true,true);
//        String ip = "http://172.21.212.124:8000/";
//        String data = HttpUtils.GET(ip + "get-models-info");
//        ObjectMapper objectMapper = new ObjectMapper();
//        List<Map> models = objectMapper.readValue(data, new TypeReference<List<Map>>() {});
//
//        mongoDBOperator.update(new ModelLibrary(),"7e483b88-c528-4ce8-bbf5-acc2b5ba5e47","models", models);
//        System.out.println("debug");

    }

}
