package com.example.moudlelibrary.config;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author TRY
 * @Date 2023/4/7 16:17
 * @Version 1.0
 */
@Configuration
public class DockerConfig {
    @Bean
    public DockerClient dockerClient() {
        DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
//                .withDockerHost("tcp://172.21.252.56:2375")
                .withDockerHost("tcp://127.0.0.1:2375")
                .build();
        return DockerClientBuilder.getInstance(config).build();
    }
}
