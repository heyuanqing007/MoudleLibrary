package com.example.moudlelibrary.tools;

/**
 * Created by HeYuanqing on 2024/06/29  16:24
 **/
public class CustomHttpRequestException extends Exception {
    public CustomHttpRequestException(String message) {
        super(message);
    }

    public CustomHttpRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
