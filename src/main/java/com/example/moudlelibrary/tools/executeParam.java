package com.example.moudlelibrary.tools;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author TRY
 * @Date 2023/5/8 14:34
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class executeParam {
    String Pre;
    String value;
}
