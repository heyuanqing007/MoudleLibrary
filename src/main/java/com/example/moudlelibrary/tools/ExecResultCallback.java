package com.example.moudlelibrary.tools;

import com.github.dockerjava.api.model.Frame;
import com.github.dockerjava.api.model.StreamType;
import com.github.dockerjava.core.command.ExecStartResultCallback;
import org.springframework.web.socket.TextMessage;

import java.util.ArrayList;

/**
 * @Author TRY
 * @Date 2023/4/12 16:02
 * @Version 1.0
 */
/**
 * @Author TRY
 * @Date 2023/4/12 16:02
 * @Version 1.0
 */
public class ExecResultCallback extends ExecStartResultCallback {
   ResultMessage resultMessage;
    public ExecResultCallback(ResultMessage resultString) {
        super(System.out, System.err);
        this.resultMessage=resultString;

    }

    @Override
    public void onNext(Frame frame) {
        if (frame.getStreamType()== StreamType.STDOUT) {
            String message = new String(frame.getPayload());
            TextMessage textMessage = new TextMessage(message);
            if (message.contains("ERROR")){
                resultMessage.setType("success");
            }else {
                resultMessage.setType("error");
            }

            resultMessage.getContent().add(message);
        } else if (frame.getStreamType()== StreamType.STDERR) {
            String message = new String(frame.getPayload());
            TextMessage textMessage = new TextMessage(message);

                resultMessage.setType("error");


            resultMessage.getContent().add(message);
        }
        super.onNext(frame);
    }
}
