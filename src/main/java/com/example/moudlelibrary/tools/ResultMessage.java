package com.example.moudlelibrary.tools;

import lombok.Data;

import java.util.List;

/**
 * Created by HeYuanqing on 2023/11/01  20:00
 **/
@Data
public class ResultMessage {
    String type;
    List<String> content;

    public ResultMessage(String type, List<String> content) {
        this.type=type;
        this.content=content;
    }
}
