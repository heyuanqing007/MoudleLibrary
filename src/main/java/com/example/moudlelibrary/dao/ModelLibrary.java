package com.example.moudlelibrary.dao;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

/**
 * Created by HeYuanqing on 2024/06/24  15:27
 **/
@Document(collection = "modelLibraries")
public class ModelLibrary {
    private String id;
    private String ip;
    private String developer;
    private String email;
    private List<Map> models;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Map> getModels() {
        return models;
    }

    public void setModels(List<Map> models) {
        this.models = models;
    }
}
