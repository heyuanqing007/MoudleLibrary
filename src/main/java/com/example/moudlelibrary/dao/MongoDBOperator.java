package com.example.moudlelibrary.dao;

import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Created by HeYuanqing on 2023/09/06  17:06
 **/
@Component
public class MongoDBOperator {
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * @param document 需要插入的文档
     * @return: void
     * @Description: 插入数据
     * @Author: HeYuanqing
     * @Date: 2023/9/6 17:24
     */
    public void insertDocument(Object document) {
        System.out.println("插入单条数据: " + document.toString());
        mongoTemplate.save(document);
    }

    /**
     * @param entityClass 查询的实体类模板
     * @param key         查询的字段
     * @param value       查询的字段值
     * @return: java.lang.Object
     * @Description: 根据字段和字段值查询
     * @Author: HeYuanqing
     * @Date: 2023/9/6 21:00
     */
    public Object findOneByKey(Object entityClass, String key, String value) {
        System.out.println("查询数据库单条记录: key = " + key + " value = " + value);
        Query query = new Query(Criteria.where(key).is(value));
        return mongoTemplate.findOne(query, entityClass.getClass());
    }

    /**
     * @param entityClass 实体类模板
     * @return: java.util.List<? extends java.lang.Object>
     * @Description: 查询所有记录
     * @Author: HeYuanqing
     * @Date: 2023/9/6 20:59
     */
    public List<? extends Object> finAll(Object entityClass) {
        System.out.println("查询数据库所有记录");
        return mongoTemplate.findAll(entityClass.getClass());
    }

    public boolean update(Object entityClass, String id, String key, Object value) {

        System.out.println("更新数据库单条记录: key = " + key + " value = " + value);
        Query query = new Query(Criteria.where("id").is(id));
        Object result = mongoTemplate.findOne(query, entityClass.getClass());
        if (result == null) {
            System.out.println(entityClass.toString() + ": " + id + " 该数据不存在！");
            return false;
        }
        Update update = new Update();
        update.set(key, value);
        mongoTemplate.updateFirst(query, update, entityClass.getClass());
        return true;

    }

       /**
     * 根据类类型、指定字段名和字段值删除特定实体。
     *
     * @param entityClass 实体类的Class对象，用于指定要删除的实体类型。
     * @param key 数据库中用于匹配记录的字段名。
     * @param value 字段key对应的值，用于定位要删除的实体。
     * @return 如果删除成功，则返回true；如果实体不存在或删除失败，则返回false。
     */
    public boolean delete(Object entityClass, String key, String value){
        // 创建查询条件，根据用户提供的字段名和值查找实体。
        Query query = new Query(Criteria.where(key).is(value));
        // 根据查询条件尝试获取实体。
        Object result =  mongoTemplate.findOne(query, entityClass.getClass());
        // 检查实体是否存在，如果不存在，则输出提示信息并返回false。
        if (result == null){
            System.out.println(entityClass.toString() + ": " +value + " 该数据不存在！");
            return false;
        }
        // 执行删除操作。
        DeleteResult deleteResult = mongoTemplate.remove(query,entityClass.getClass());
        // 检查删除操作是否成功，如果删除成功，则输出提示信息并返回true；否则输出失败信息并返回false。
        if (deleteResult.getDeletedCount() > 0) {
            System.out.println(entityClass.toString() + ": " + value + " 删除成功！");
            return true;
        } else {
            System.out.println(entityClass.toString() + ": " + value + " 删除失败！");
            return false;
        }
    }

}
