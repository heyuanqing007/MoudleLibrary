package com.example.moudlelibrary.dao;

/**
 * Created by HeYuanqing on 2023/11/06  14:59
 **/
public enum DataType {
    GeoData, //栅格和矢量地理数据
    File, //文件类型数据

}
