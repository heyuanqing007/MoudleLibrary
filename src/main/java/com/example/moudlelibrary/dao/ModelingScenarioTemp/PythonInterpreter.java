package com.example.moudlelibrary.dao.ModelingScenarioTemp;

/**
 * Created by HeYuanqing on 2023/10/26  16:33
 **/
public enum PythonInterpreter {
    Python3_7(0, "python:3.7-bullseye"),
    Python3_8(1, "python:3.8-bullseye"),
    Python3_9(2, "python:3.9-bullseye");
    private int index;
    private String tag;
    PythonInterpreter(int index, String tag) {
        this.index = index;
        this.tag = tag;
    }
    public String getTag(){
        return tag;
    }
    public static PythonInterpreter[] getValues(){
        return values();
    }
}
