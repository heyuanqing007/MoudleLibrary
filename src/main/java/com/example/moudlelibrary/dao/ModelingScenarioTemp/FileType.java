package com.example.moudlelibrary.dao.ModelingScenarioTemp;

/**
 * Created by HeYuanqing on 2022/11/22  19:37
 **/
public enum FileType {
    pdf, gif, zip, txt, shp, tif,csv,
    excel, ppt, word, audio, video, image,
    folder,
    unknown

}
