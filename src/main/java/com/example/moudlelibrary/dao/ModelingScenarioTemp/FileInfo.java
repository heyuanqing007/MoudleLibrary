package com.example.moudlelibrary.dao.ModelingScenarioTemp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * Created by HeYuanqing on 2022/11/21  20:25
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FileInfo {
    /**
     * 文件夹或文件名
     */
    private String fileName;
    /**
     * 相对路径
     */
    private String filePath;
    /**
     * 绝对路径
     */
    private String absolutePath;
    /**
     * 文件大小
     */
    private Long fileSize;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 文件中子文件
     */
    private List<FileInfo> childrenFileList;
    /**
     * 文件类型
     */
    private FileType fileType;

}
