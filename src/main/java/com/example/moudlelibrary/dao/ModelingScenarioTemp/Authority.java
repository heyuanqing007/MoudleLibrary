package com.example.moudlelibrary.dao.ModelingScenarioTemp;

/**
 * Created by HeYuanqing on 2023/10/26  16:28
 **/
public enum Authority {
    PRIVATE, PUBLIC
}
