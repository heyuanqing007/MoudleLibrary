package com.example.moudlelibrary.dao;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by HeYuanqing on 2023/09/21  19:32
 **/
@Document(collection = "dockerContainers")
public class DockerContainer {
    private String id;
    private String name;
    private String containerID;
    private String imageName;
    private String volumesDir;
    private String workDir;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContainerID() {
        return containerID;
    }

    public void setContainerID(String containerID) {
        this.containerID = containerID;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getVolumesDir() {
        return volumesDir;
    }

    public void setVolumesDir(String volumesDir) {
        this.volumesDir = volumesDir;
    }

    public String getWorkDir() {
        return workDir;
    }

    public void setWorkDir(String workDir) {
        this.workDir = workDir;
    }
}
