package com.example.moudlelibrary.dao;

import com.example.moudlelibrary.dao.ModelingScenarioTemp.Authority;
import com.example.moudlelibrary.dao.ModelingScenarioTemp.PythonInterpreter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by HeYuanqing on 2023/10/26  16:16
 **/
@Document(collection = "modelingScenarios")
public class ModelingScene {
    private String id;
    private String name;
    private PythonInterpreter interpreter;
    private String desc;
    private String developer;
    private String email;
    private Authority authority;
    private String containerID;
    private String code;
    private String[] library;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PythonInterpreter getInterpreter() {
        return interpreter;
    }

    public void setInterpreter(PythonInterpreter interpreter) {
        this.interpreter = interpreter;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Authority getAuthority() {
        return authority;
    }

    public void setAuthority(Authority authority) {
        this.authority = authority;
    }

    public String getContainerID() {
        return containerID;
    }

    public void setContainerID(String containerID) {
        this.containerID = containerID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String[] getLibrary() {
        return library;
    }

    public void setLibrary(String[] library) {
        this.library = library;
    }
}
