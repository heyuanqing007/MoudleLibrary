package com.example.moudlelibrary.dao;

/**
 * Created by HeYuanqing on 2023/11/09  14:32
 **/
public enum ModuleType {
    Hydrology, Pedology, Meteorology, Ecology, Socioeconomics, Others
}
