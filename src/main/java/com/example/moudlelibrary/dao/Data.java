package com.example.moudlelibrary.dao;

import com.example.moudlelibrary.dao.ModelingScenarioTemp.Authority;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoProperties;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


/**
 * Created by HeYuanqing on 2023/11/06  14:55
 **/
@Document(collection = "Data")
public class Data {
    private String id;
    private String name;
    private String description;
    private String modelingSceneID;
    private DataType type;
    private String path;
    private Authority authority;
}
