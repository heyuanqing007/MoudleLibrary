package com.example.moudlelibrary.dao;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

/**
 * Created by HeYuanqing on 2023/09/06  16:57
 **/
@Document(collection = "modules")
public class Module {
    private String id;
    private String name;
    private String description;
    private ModuleType type;
    private String[] keyword;
    private String provider;
    private String email;
    private String copyright;
    private List<Map> inputData;
    private List<Map> outputData;
    private String fileScheme;
    private String dataScheme;
    private String codeExample;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ModuleType getType() {
        return type;
    }

    public void setType(ModuleType type) {
        this.type = type;
    }

    public String[] getKeyword() {
        return keyword;
    }

    public void setKeyword(String[] keyword) {
        this.keyword = keyword;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public List<Map> getInputData() {
        return inputData;
    }

    public void setInputData(List<Map> inputData) {
        this.inputData = inputData;
    }

    public List<Map> getOutputData() {
        return outputData;
    }

    public void setOutputData(List<Map> outputData) {
        this.outputData = outputData;
    }

    public String getFileScheme() {
        return fileScheme;
    }

    public void setFileScheme(String fileScheme) {
        this.fileScheme = fileScheme;
    }

    public String getDataScheme() {
        return dataScheme;
    }

    public void setDataScheme(String dataScheme) {
        this.dataScheme = dataScheme;
    }

    public String getCodeExample() {
        return codeExample;
    }

    public void setCodeExample(String codeExample) {
        this.codeExample = codeExample;
    }
}
