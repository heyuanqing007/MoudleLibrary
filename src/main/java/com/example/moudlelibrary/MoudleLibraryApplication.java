package com.example.moudlelibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoudleLibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoudleLibraryApplication.class, args);
    }

}
